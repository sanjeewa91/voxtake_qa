'''
Created on Jun 26, 2017

@author: alpha
'''
from framework.services.Premiumcode.premiumcode_services import PremiumcodeServices
from nose.tools import assert_equals
from framework.storage.dao.daoPlanmanagement import DaoPlanManagement



getallservices = PremiumcodeServices()
dao_plans= DaoPlanManagement()

class TestPlanManagementGet(object):
    '''
    classdocs
    '''



    def setup(self):
   
        pass
    
    
    def testget_allPlans(self):

        response = getallservices.getAllplans()

        db_data = dao_plans.get_plans()
        
        assert_equals(response[0].status_code, 200)
        assert_equals(response[1]['totalRecords'], len(db_data))
        
        
     
    def testLimited_plans(self):
        parameters = {"page":1,"start":0,"limit":5}
    
        response = getallservices.getAllplans(parms=parameters)
        
        assert_equals(response[0].status_code, 200)   
        assert_equals(len(response[1]['data']), 5)
       
       
     

    
        
        
 
        
        
        