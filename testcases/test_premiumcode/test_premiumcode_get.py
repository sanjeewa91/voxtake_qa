'''
Created on Jun 14, 2017

@author: alpha
'''

from framework.services.Premiumcode.premiumcode_services import PremiumcodeServices
from nose.tools import assert_equals
from framework.storage.dao.daoPremium_codes import DaoPremiumCodes



dao_premiumcodes = DaoPremiumCodes()
getallservices = PremiumcodeServices()


class TestPremiumGet(object):
    '''
    classdocs
    '''


    def setup(self):
   
        pass
    
    
    def testget_allPremiumCodes(self):

        
        response = getallservices.getPremium()
        
        db_data = dao_premiumcodes.get_premiumcode_by_seq()
        
        
        ''''Test to verify get all premium codes from api '''
        
        assert_equals(response[0].status_code, 200)
        assert_equals(response[1]['totalRecords'], len(db_data)) 
        
        
    def testLimited_premium(self):
        parameters = {"page":1,"start":0,"limit":2}
    
        response = getallservices.getPremium(parms=parameters)
        
        ''''Test to verify limits premium codes from api '''
        assert_equals(response[0].status_code, 200)   
        assert_equals(len(response[1]['data']), 2)
        

        
      
   
   
   
   
   
   
            
            
            
    
        
        