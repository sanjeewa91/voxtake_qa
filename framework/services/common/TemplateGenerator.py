'''
Created on Jun 19, 2017

@author: alpha
'''
from pkg_resources import ResourceManager
import jinja2
import json

class TemplateGenerater(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
          
    def generate_template(self, template_dict=None, path=None):
        '''
            Populate template using given data
            @param template_dict: dict - jinja template. 
            @param path: str - path to template.
            @return: Json message -  rendered template        
        '''
        
        
        rm = ResourceManager()
        templateLoader = jinja2.FileSystemLoader( searchpath="/" )
        templateEnv = jinja2.Environment( loader=templateLoader )
        templateEnv.filters['jsonify'] = json.dumps
                
        template_location = rm.resource_filename('framework', path)
        raw_template = templateEnv.get_template(template_location)

        message = raw_template.render(template_dict)  

        return message     