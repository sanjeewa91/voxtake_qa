'''
Created on Jun 29, 2017

@author: alpha
'''
from testconfig import config
class UrlGenerator(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.urls = {
                     
                     'SAVE_PREMIUM':'/PremiumCodes/addPremiumCode?',
                     
                     }
        
    def get_url(self, api):
        url = config['VOX_CMS']['URL'] + self.urls[api]
        return url