




import requests



class RestClient(object):
    '''
        Parent Class for dealing with generic POSTS/UPDATES/GETS/DELETES
    '''
    global json_header, response, timeout
    response = None
    timeout = 600


    def __init__(self):
        '''
        Constructor
        '''

                
    def put(self, url = None, data = None, params = None, hdrs = None, ck = None, basic_auth = None):
                
        response  = requests.put(url, data = data,verify=False, params = params, headers = hdrs, cookies = ck, auth = basic_auth, timeout=timeout)    
        
        return response
                
    def get(self, url =  None, params = None, hdrs = None, ck = None, basic_auth = None):
        
        response = requests.get(url, params = params, verify=False,headers = hdrs, cookies = ck, auth = basic_auth, timeout=timeout)

        return response
        
    def delete(self, url, params = None, hdrs = None, ck = None, basic_auth = None):
        
        response = requests.delete(url, params = params, headers = hdrs, cookies = ck, auth = basic_auth, timeout=timeout) 
        
        return response
        
    def update(self, url, data = None, params = None, hdrs = None, ck = None, basic_auth = None):
        
        response = requests.put(url, data = data, params = params, headers = hdrs, cookies = ck, auth = basic_auth, timeout=timeout)
        
        return response
