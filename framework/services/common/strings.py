'''
Created on Jun 20, 2017

@author: alpha
'''
import random
import string

def generateHost(min=None,max=None):
#     charSet = 'abcdefghijklmnopqrstuvwxyz1234567890'
    minLength = min
    maxLength = max
    length = random.randint(minLength, maxLength)

    return ''.join(random.choice(string.ascii_uppercase) for _ in range(length))