'''
Created on Jun 13, 2017

@author: alpha
'''
from framework.objects.premiumcode import PremiumCode
from framework.services.Premiumcode.premiumcode_services import PremiumcodeServices
from framework.testdata.premiumcode import premium_code
import random
from framework.services.common.UrlGenerator import UrlGenerator
from framework.services.common.TemplateGenerator import TemplateGenerater

premium_service = PremiumcodeServices()


class PremiumCodeClient(PremiumcodeServices):
    '''
    classdocs
    '''
    
    def __init__(self):
        self.template_generator = TemplateGenerater()
        self.urlgen = UrlGenerator()
        
        
        
    
    def createPremiumcode(self, premium_code=None):
        
        if premium_code == None:
            self.premium_code = PremiumCode()
        else:
            self.premium_code = premium_code
            
           
        response = self.createPremium(self.premium_code)
    
        return response, self.premium_code
        
        
    
    def createPremiumobject(self):
        
        premium_code = PremiumCode()
        
        premium_code.seq=0
        premium_code.country = "Ireland"
        premium_code.code = random.randint(600,700)
        premium_code.status = 1
        premium_code.user = ""
        

        
        return premium_code