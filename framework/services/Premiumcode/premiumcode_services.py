'''
Created on Jun 13, 2017

@author: alpha
'''
from framework.services.common.rest_client import RestClient
from collections import OrderedDict
from urllib import urlencode
from framework.services.common.UrlGenerator import UrlGenerator

import json
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class PremiumcodeServices(RestClient):
    '''
    classdocs
    '''


    def __init__(self):
               
        '''
        Constructor
        ''' 
        self.urlgen = UrlGenerator()
        
    def getAllplans(self, parms = {"page":1,"start":0,"limit":20}):
        logger.info('Plan management: get all plans ....')     
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/PlanManagements/getPlanManagements", params = parms,hdrs={'Content-Type':'application/json'})
        logger.info('Plan management: get all plan response %s' % response.content)
        return response, json.loads(response.content)
    
    def getAllpartner(self, parms = {"page":1,"start":0,"limit":20}):
        logger.info('Partner management: get all partners ....')     
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/Partners/getPartners", params = parms,hdrs={'Content-Type':'application/json'})
        logger.info('Partner management: get all partners response %s' % response.content)
        return response, json.loads(response.content)
    
    def getAllpromoter(self, parms = {"page":1,"start":0,"limit":20}):
        logger.info('Promoter management: get all promoter ....')     
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/Promoters/getPromoters", params = parms,hdrs={'Content-Type':'application/json'})
        logger.info('Promoter management: get all promoter response %s' % response.content)
        return response, json.loads(response.content)
    
    
    def getAllpromotion(self, parms = {"page":1,"start":0,"limit":20}):
        logger.info('Promotion management: get all Promotion ....')     
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/Promotions/getPromotions", params = parms,hdrs={'Content-Type':'application/json'})
        logger.info('Promotion management: get all Promotion response %s' % response.content)
        return response, json.loads(response.content)
    
    def getPremium(self, parms = {"clientId":"","page":1,"start":1,"limit":20}):
        logger.info('Premium code: get all premiums ....')        
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/PremiumCodes/getPremiumCodes", params=parms, hdrs={'Content-Type':'application/json'})
        logger.info('Premium code: get all premiums response %s' % response.content)
        return response, json.loads(response.content)
        
    
    def getAllnewtaptag(self, parms = {"clientId":"","page":1,"start":1,"limit":20}):
        logger.info('NewTaptag : get all NewTaptag ....')     
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/MyTaptags/getMyTaptags", params = parms,hdrs={'Content-Type':'application/json'})
        logger.info('NewTaptag : get all NewTaptag response %s' % response.content)
        return response, json.loads(response.content)
    
    def getAlltaptagmanagement(self, parms = {"clientId":"","page":1,"start":1,"limit":20}):
        logger.info('Taptag management: get all Taptag ....')     
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/Users/getClientTaptag", params = parms,hdrs={'Content-Type':'application/json'})
        logger.info('Taptag management: get all Taptag response %s' % response.content)
        return response, json.loads(response.content)
    
    def getAllserviceprovider(self, parms = {"page":1,"start":1,"limit":20}):
        logger.info('Service provider management: get all Service provider ....')     
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/ServiceProviders/getServiceProviders", params = parms,hdrs={'Content-Type':'application/json'})
        logger.info('Service provider management: get all Service provider response %s' % response.content)
        return response, json.loads(response.content)
            
   
    def createPremium(self, premium_code=None):
        logger.info('Premium code: create premium ....')        
        create_premiumcode_template = self.template_generator.generate_template(template_dict={"premiumcd": premium_code}, path='templates/premiumcode/premiumcode.json')
        logger.info('Premium codet: create premium: %s' % create_premiumcode_template)    
        response =super(PremiumcodeServices, self).put(url=self.urlgen.get_url('SAVE_PREMIUM'), data=create_premiumcode_template, hdrs={'Content-Type':'application/json'})
        logger.info('Premium code: create premium response %s' % response.content)
        return response, json.loads(response.content)
    
    
    
    def getPremiumbyseq(self,seq):
        parms = OrderedDict([("clientId", seq), ("start", 1), ("limit", 100)])
        logger.info('Premium codes: get all premiumcodes ....')        
        response = super(PremiumcodeServices, self).get(url="http://192.0.0.222:8080/VoxtakePortalWebServiceTest-1.0/Services/PremiumCodes/getPremiumCodes", params=urlencode(parms), hdrs={'Content-Type':'application/json'})
        logger.info('Premium codes: get all premium response %s' % response.content)
        return response, json.loads(response.content)
    
    
    
    
    
 
    