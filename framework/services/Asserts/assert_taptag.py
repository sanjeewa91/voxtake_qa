'''
Created on Jun 13, 2017

@author: alpha
'''

from nose.tools import assert_equals
class AssertTaptag(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    def assert_taptag(self, response, taptags):
        '''
            Assert location response against location object
        '''   
        assert_equals(str(response["taptag"]) ,str(taptags.taptag))
        assert_equals(str(response["clientId"]) ,str(taptags.clientId))
        assert_equals(str(response["clientName"]) ,str(taptags.clientName))
        assert_equals(str(response["status"]) ,str(taptags.status))
        