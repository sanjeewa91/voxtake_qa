'''
Created on Jun 26, 2017

@author: alpha
'''
from framework.storage.mysql_client import MySqlClient
from testconfig import config
from _mysql import result
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)

class DaoPlanManagement(object):
    '''
    classdocs
    '''
    def __init__(self, conf = None):
        '''
            Constructor
            Set Mysql config.
        '''
        if conf:
            self.conf = conf
        else:
            self.conf =  config['mysql']
            
        self.db = MySqlClient(self.conf)
                            
    def connect(self, conf=None):
        
        if self.db:
            try:
                self.db.close()
            except:
                raise Exception, 'Unable to close'
                        
        self.db = MySqlClient(self.conf)
        logger.info('DB connected')
        
        
    def get_plans(self):
        self.connect()
        
        try:
            query = "SELECT PlanName FROM plan_management; " 
            logger.info('Query : %s',query) 
            result = self.db.query(query, True, False)
            logger.info('Result : %s',result)  
            self.db.close()
            
            return result
        
        except:
            raise Exception('Unable to get  db data')
    
    
        