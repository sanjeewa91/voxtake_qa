'''
Created on Jun 16, 2017

@author: alpha
'''
from framework.storage.mysql_client import MySqlClient
from testconfig import config
from _mysql import result
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)

class DaoPremiumCodes(object):
    '''
    classdocs
    '''
    def __init__(self, conf = None):
        '''
            Constructor
            Set Mysql config.
        '''
        if conf:
            self.conf = conf
        else:
            self.conf =  config['mysql']
            
        self.db = MySqlClient(self.conf)
                            
    def connect(self, conf=None):
        
        if self.db:
            try:
                self.db.close()
            except:
                raise Exception, 'Unable to close'
                        
        self.db = MySqlClient(self.conf)
        logger.info('DB connected')
        
                        
    def get_premiumcode_by_seq(self):
        '''
            Selects premiumcode attributes by seq
           
        '''
        self.connect()
        try:
            query = "SELECT seq, country  FROM premium_codes " 
            logger.info('DB-query : %s',query) 
            result = self.db.query(query, True, False)
            logger.info('DB-result : %s',result)  
            self.db.close()
            
            return result
        
        except:
            raise Exception('Unable to get premium db data')
        
 