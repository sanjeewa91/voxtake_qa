'''
Created on Jun 16, 2017

@author: alpha
'''
from MySQLdb.constants import FIELD_TYPE

import MySQLdb
import warnings

class MySqlClient(object):
    '''
    classdocs
    '''


    def __init__(self, conn):
        '''
        Constructor
        '''
        self.conn = conn
     

        
        self.con = MySQLdb.connect(host = self.conn['host'], user = self.conn['username'], passwd = self.conn['password'], db = self.conn['db'], port = int(self.conn['port']) if 'port' in self.conn else 3306)

            
    def close(self):
        self.con.close() 
    
    def query(self, qs, wrap = False, list_results = False):
        '''
            Perform query
            @param qs: query string to be executed
            @return: tuple (fetchall(), description)
        '''
        if not self.con:
            raise Exception, 'No DB Conneciton'
        try:
            c = self.con.cursor()
            c.execute(qs)
            values = c.fetchall()
            description = c.description
        finally:
            c.close()
        
        if wrap:
            return wrap_results_with_dict(values,description)
        if list_results:
            return add_results_to_list(values)
        else:
            return values, description
        
    def query_with_commit(self, qs, wrap = False, list_results = False, get_id = False):
        '''
            Perform query
            @param qs: query string to be executed
            @return: tuple (fetchall(), description)
        '''

        id = None
        
        if not self.con:
            raise Exception, 'No DB Conneciton'
        
        
        values = None
        description = None
        
        try:
            c = self.con.cursor()
            c.execute(qs)
            id = c.lastrowid
            values = c.fetchall()
            description = c.description        

            self.con.commit()

        except Exception, e:
            print e
            if str(e) not in ('no results', 'No data -zero rows fetched,selected, or processed'):
                self.con.rollback()
                raise e
        finally:
            c.close()
        
        if get_id:
            return id
    
        if wrap:
            return wrap_results_with_dict(values, description)
        if list_results:
            return add_results_to_list(values)
        else:
            return values, description
                
def wrap_results_with_dict(results, description):
    '''
        Create dictonary from resultset
        
        @param: results: list of results
        @param: description of rows in results
        @return: list - results wrapped in dictionary
    '''
    
    dict_results = []
    for row in results:
        dict_results.append(dict(zip(tuple([columns[0] for columns in description]), row)))
             
    return dict_results 

def add_results_to_list(results):
    '''
        All results set are added to list
        
        @param: results: list of results
        @return: list - results
    '''
    
    list_results = []
    for row in results:
        list_results.append(row[0])
             
    return list_results            